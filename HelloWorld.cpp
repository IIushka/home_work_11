﻿#include <iostream>

int main()
{    
    std::string word = "privet";

    std::cout << word << std::endl;
    std::cout << word.length() << std::endl;
    std::cout << word[0] << std::endl;
    std::cout << word[word.length()-1] << std::endl;

    return 0;
}
